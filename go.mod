module goRestful

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	go.mongodb.org/mongo-driver v1.3.1
)
