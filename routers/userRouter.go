package routers

import (
	userControllers "goRestful/controllers"

	"github.com/gin-gonic/gin"
)

func UserRouter() *gin.Engine {
	router := gin.Default()
	router.POST("/UserLogin", userControllers.Login)           // 登入
	router.GET("/GetUser/:userid", userControllers.GetUser)    // 取得特定使用者資料
	router.POST("/AddUser", userControllers.AddUser)           // 新增使用者
	router.DELETE("/DelUser/:userid", userControllers.DelUser) // 刪除使用者
	router.PUT("/UpdUser/:userid", userControllers.UpdUser)    // 更新使用者名稱
	return router
}
