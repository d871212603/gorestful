package controllers

import (
	userModel "goRestful/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive" // for BSON ObjectID
)

// 登入
func Login(c *gin.Context) {
	var user userModel.User
	user.Acc = c.PostForm("acc")
	user.Pwd = c.PostForm("pwd")

	result, err := user.Login()

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code":    0,
			"message": "登入失敗",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 1,
		"data": result,
	})
}

// 取得特定使用者資料
func GetUser(c *gin.Context) {
	userID, err := primitive.ObjectIDFromHex(c.Param("userid"))
	result, err := userModel.Getuser(userID)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code":    0,
			"message": "登入失敗",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 1,
		"data": result,
	})
}

// 新增
func AddUser(c *gin.Context) {
	var user userModel.User
	user.Username = c.PostForm("username")
	user.Acc = c.PostForm("acc")
	user.Pwd = c.PostForm("pwd")

	result, err := user.AddUser()

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code":    0,
			"message": "新增使用者失敗",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 1,
		"data": result,
	})
}

// 刪除
func DelUser(c *gin.Context) {
	userID, err := primitive.ObjectIDFromHex(c.Param("userid"))
	result, err := userModel.DelUser(userID)

	if err != nil || result.DeletedCount != 1 {
		c.JSON(http.StatusOK, gin.H{
			"code":    0,
			"message": "刪除使用者失敗",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 1,
		"data": result,
	})
}

// 更新
func UpdUser(c *gin.Context) {
	var user userModel.User
	userID, err := primitive.ObjectIDFromHex(c.Param("userid"))

	user.Username = c.PostForm("username")
	user.Pwd = c.PostForm("pwd")

	result, err := user.UpdUser(userID)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code":    0,
			"message": "更新使用者失敗",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 1,
		"data": result,
	})
}
