package models

import (
	"context"
	con "goRestful/configs"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive" // for BSON ObjectID
	"go.mongodb.org/mongo-driver/mongo"
)

// mongoDB(user) model
type User struct {
	//ID primitive.ObjectID `bson:"_id"`
	Username string `json:"username"`
	Acc      string `json:"acc"`
	Pwd      string `json:"pwd"`
}

type ID struct {
	ID primitive.ObjectID `bson:"_id"`
}

// 使用者登入
func (user *User) Login() (id ID, err error) {
	// DB 連線
	db := con.ConnectDB()
	// 連接到user collection(mongoDB)
	collection := db.Database("TEST").Collection("user")

	fail := collection.FindOne(context.TODO(), bson.M{"acc": user.Acc, "pwd": user.Pwd}).Decode(&id)

	// 關閉 DB 連線
	db.Disconnect(context.TODO())

	if fail != nil {
		err = fail
		return
	}

	return
}

// 取得特定使用者資料(不含_id)
func Getuser(userID primitive.ObjectID) (data User, err error) {
	// DB 連線
	db := con.ConnectDB()
	// 連接到user collection(mongoDB)
	collection := db.Database("TEST").Collection("user")

	fail := collection.FindOne(context.TODO(), bson.M{"_id": userID}).Decode(&data)
	// 關閉 DB 連線
	db.Disconnect(context.TODO())

	if fail != nil {
		err = fail
		return
	}

	return
}

// 新增使用者
func (user *User) AddUser() (id *mongo.InsertOneResult, err error) {
	// DB 連線
	db := con.ConnectDB()
	// 連接到user collection(mongoDB)
	collection := db.Database("TEST").Collection("user")

	id, fail := collection.InsertOne(context.TODO(), user)

	// 關閉 DB 連線
	db.Disconnect(context.TODO())

	if fail != nil {
		err = fail
		return
	}

	return
}

// 刪除使用者
func DelUser(userID primitive.ObjectID) (res *mongo.DeleteResult, err error) {
	// DB 連線
	db := con.ConnectDB()
	// 連接到user collection(mongoDB)
	collection := db.Database("TEST").Collection("user")

	res, fail := collection.DeleteOne(context.TODO(), bson.M{"_id": userID})

	// 關閉 DB 連線
	db.Disconnect(context.TODO())

	if fail != nil {
		err = fail
		return
	}

	return
}

// 更新使用者資料
func (user *User) UpdUser(userID primitive.ObjectID) (res *mongo.UpdateResult, err error) {
	// DB 連線
	db := con.ConnectDB()
	// 連接到user collection(mongoDB)
	collection := db.Database("TEST").Collection("user")

	res, fail := collection.UpdateOne(context.TODO(),
		bson.M{"_id": userID},
		bson.D{
			{"$set", bson.M{"username": user.Username}},
		})

	// 關閉 DB 連線
	db.Disconnect(context.TODO())

	if fail != nil {
		err = fail
		return
	}

	return
}
