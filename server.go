package main

import (
	userRouter"goRestful/routers"
)


func main() {
	userRouter := userRouter.UserRouter()
	userRouter.Run(":3000")
}
